#include <QtWidgets>

#include "gitpp7.h"
#include "checkouttab.h"

CheckoutTab::CheckoutTab(QWidget *parent)
    : QWidget(parent)
{
  branchListBox = new QListWidget;

  GITPP::REPO r;
  branch_count = 0;
  for(auto i : r.branches()){
    branch_count++;
    branchListBox->insertItem(0,i.name().c_str());
	}

  //create button
  checkout_button = new QPushButton("Checkout", this);
  checkout_button->setFixedSize(QSize(100, 30));
  connect(checkout_button, SIGNAL (released()), this, SLOT (checkoutBranch()));

  //check for branches
  if (branch_count == 0){
    checkout_button->setEnabled(false);
    branchListBox->insertItem(0,"No branches in git repository");
  }

  //create layout
  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(branchListBox);
  layout->addWidget(checkout_button);
  setLayout(layout);
}

void CheckoutTab::checkoutBranch()
{
  current_branch = branchListBox->currentItem()->text().toStdString();

  GITPP::REPO r;
  r.checkout(current_branch);

  //create pop up window on successfull branch checkout
  msgbox = new QMessageBox(this);
  msgbox->setWindowTitle("Git interface");
  msgbox->setText("Successfully changed branches");
  msgbox->open();
}
