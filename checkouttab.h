#include <QtWidgets>

class CheckoutTab : public QWidget
{
  Q_OBJECT

public:
  explicit CheckoutTab(QWidget *parent = nullptr);

private:
  int branch_count;
  std::string current_branch;

  QMessageBox *msgbox;
  QListWidget *branchListBox;
  QPushButton *checkout_button;

private slots:
    void checkoutBranch();
};
