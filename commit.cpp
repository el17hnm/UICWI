#include <QtWidgets>

#include "gitpp7.h"
#include "commit.h"

CommitHistoryTab::CommitHistoryTab(QWidget *parent)
    : QWidget(parent)
{
  branchListBox = new QListWidget;

  GITPP::REPO r;

  QVBoxLayout *area = new QVBoxLayout;
  QTextEdit *text = new QTextEdit();
  std::string message;

  text->setText("LIST OF COMMITS:\n");
  for(auto i : r.commits())
  {
    message = i.message();
    QString qmessage = QString::fromStdString(message);
    text->append(qmessage);
  }
  text->setReadOnly(true);

  area->addWidget(text);
  setLayout(area);
}
