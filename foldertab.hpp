#include <QtWidgets>

class FolderTab : public QWidget
{
  Q_OBJECT

public:
  explicit FolderTab(QWidget *parent = nullptr);

private:
  QMessageBox *msgbox;
  QListWidget *branchListBox;
  QPushButton *checkout_button;

private slots:
    void dialog();
};
