#include <QtWidgets>
#include <string>
#include <iostream>


#include "gitpp7.h"
#include "keywordsearch.hpp"

KeywordSearch::KeywordSearch(QWidget *parent)
    : QWidget(parent)
{
  GITPP::REPO r;

  //create linedit
  searchInputBox = new QLineEdit();


  //create button
  searchButton = new QPushButton("Search", this);
  searchButton->setFixedSize(QSize(100, 30));
  connect(searchButton, SIGNAL (released()), this, SLOT (keywordSearch()));

  //create layout
  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(searchInputBox);
  layout->addWidget(searchButton);
  setLayout(layout);
}

void KeywordSearch::keywordSearch()
{
  //create results box
  GITPP::REPO r;

  searchResultBox = new QListWidget();

  for(auto i : r.commits())
  {
    std::string message = i.message();
    // if(strstr(message, searchInputBox->text().toLocal8Bit().data())){
    if (message.find(searchInputBox->text().toUtf8().constData()) != std::string::npos) {
      searchResultBox->insertItem(0, QString::fromStdString(message));
    }

  }

  QVBoxLayout *resultLayout = new QVBoxLayout;
  resultLayout->addWidget(searchResultBox);

  QWidget *wdg = new QWidget;
  wdg->setWindowTitle(tr("Git interface"));
  wdg->setLayout(resultLayout);
  wdg->show();

}
