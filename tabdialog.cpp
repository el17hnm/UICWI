#include <QtWidgets>

#include "gitpp7.h"
#include "tabdialog.h"
#include "checkouttab.h"
#include "foldertab.hpp"
#include "keywordsearch.hpp"
#include "commit.h"

///////////////////////////////////////////////////////////////////////////////
GitInterface::GitInterface(QWidget *parent)
    : QDialog(parent)
{
  setWindowFlags(Qt::Window
    | Qt::WindowMinimizeButtonHint
    | Qt::WindowMaximizeButtonHint
    | Qt::WindowCloseButtonHint);

  tabWidget = new QTabWidget;
  tabWidget->addTab(new ExampleTab(), tr("Example"));
  tabWidget->addTab(new CheckoutTab(), tr("Branch navigation"));
  tabWidget->addTab(new FolderTab(), tr("Select Repo"));
  tabWidget->addTab(new KeywordSearch(), tr("Keyword Search"));
  tabWidget->addTab(new CommitHistoryTab(), tr("Commit History"));

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(tabWidget);
  setLayout(mainLayout);

  setWindowTitle(tr("Git interface"));
}
///////////////////////////////////////////////////////////////////////////////

ExampleTab::ExampleTab(QWidget *parent)
    : QWidget(parent)
{
  QVBoxLayout *mainLayout = new QVBoxLayout;
  setLayout(mainLayout);
}
